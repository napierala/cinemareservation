package pl.napierala.web.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.napierala.web.model.Reservation;

@Repository
public class ReservationDaoImpl implements ReservationDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addReservation(Reservation reservation) {
		getCurrentSession().save(reservation);
	}

	@Override
	public void updateReservation(Reservation reservation) {
		this.sessionFactory.getCurrentSession().update(reservation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Reservation> getReservationList() {
		List<Reservation> reservationList = getCurrentSession().createQuery("from Reservation").list();
		return reservationList;
	}

	@Override
	public Reservation getReservationById(Integer id) {
		return (Reservation) getCurrentSession().get(
				Reservation.class, id);
	}

	@Override
	public void removeReservation(Integer id) {
		Reservation reservation = getReservationById(id);
		if (null != reservation) {
			getCurrentSession().delete(reservation);
		}

	}
}
