package pl.napierala.web.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.napierala.web.model.Auditorium;

@Repository
public class AuditoriumDaoImpl implements AuditoriumDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addAuditorium(Auditorium auditorium){
		getCurrentSession().save(auditorium);
	}

	@Override
	public void updateAuditorium(Auditorium auditorium){
		getCurrentSession().update(auditorium);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Auditorium> getAuditoriumList(){
		List<Auditorium> auditoriumList = getCurrentSession().createQuery("from Auditorium")
				.list();
		return auditoriumList;
	}

	@Override
	public Auditorium getAuditoriumById(Integer id){
		return (Auditorium) getCurrentSession().get(Auditorium.class, id);
	}

	@Override
	public void removeAuditorium(Integer id){
		Auditorium auditorium = getAuditoriumById(id);
		if (null != auditorium) {
			getCurrentSession().delete(auditorium);
		}
	}
}
