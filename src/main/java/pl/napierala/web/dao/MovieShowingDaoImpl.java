package pl.napierala.web.dao;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.napierala.web.model.MovieShowing;

@Repository
public class MovieShowingDaoImpl implements MovieShowingDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addMovieShowing(MovieShowing movieShowing) {
		getCurrentSession().save(movieShowing);
	}

	@Override
	public void updateMovieShowing(MovieShowing movieShowing) {
		this.sessionFactory.getCurrentSession().update(movieShowing);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovieShowing> getMovieShowingList() {
		List<MovieShowing> movieShowingList = getCurrentSession().createQuery(
				"from MovieShowing").list();
		return movieShowingList;
	}

	@Override
	public MovieShowing getMovieShowingById(Integer id) {
		return (MovieShowing) getCurrentSession().get(MovieShowing.class, id);
	}

	@Override
	public void removeMovieShowing(Integer id) {
		MovieShowing movieShowing = getMovieShowingById(id);
		if (null != movieShowing) {
			getCurrentSession().delete(movieShowing);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovieShowing> getTodaysMovieShowings() {

		//From the very morning
		Calendar from = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
		from.set(Calendar.HOUR_OF_DAY, 0);
		from.set(Calendar.MINUTE, 0);
		from.set(Calendar.SECOND, 0);
		
		//To midnight
		Calendar to = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
		to.set(Calendar.HOUR_OF_DAY, 23);
		to.set(Calendar.MINUTE, 59);
		to.set(Calendar.SECOND, 59);
		

		List<MovieShowing> movieShowingList = getCurrentSession()
				.createQuery(
						"from MovieShowing ms where ms.showTime between :from and :to")
				.setParameter("from", from.getTime())
				.setParameter("to", to.getTime()).list();

		return movieShowingList;
	}
}
