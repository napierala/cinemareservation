package pl.napierala.web.dao;

import java.util.List;

import pl.napierala.web.model.Movie;

public interface MovieDao {
	
    public void addMovie(Movie movie);
    public void updateMovie(Movie movie);
    public List<Movie> getMovieList();
    public Movie getMovieById(Integer id);
    public void removeMovie(Integer id);
	
}
