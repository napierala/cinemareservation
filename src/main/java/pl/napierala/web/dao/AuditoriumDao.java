package pl.napierala.web.dao;

import java.util.List;

import pl.napierala.web.model.Auditorium;

public interface AuditoriumDao {
	
    public void addAuditorium(Auditorium auditorium);
    public void updateAuditorium(Auditorium auditorium);
    public List<Auditorium> getAuditoriumList();
    public Auditorium getAuditoriumById(Integer id);
    public void removeAuditorium(Integer id);
	
}
