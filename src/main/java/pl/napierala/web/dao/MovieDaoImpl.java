package pl.napierala.web.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.napierala.web.model.Movie;

@Repository
public class MovieDaoImpl implements MovieDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addMovie(Movie movie) {
		getCurrentSession().save(movie);
	}

	@Override
	public void updateMovie(Movie movie) {
		getCurrentSession().update(movie);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Movie> getMovieList() {
		List<Movie> movieList = getCurrentSession().createQuery("from Movie").list();
		return movieList;
	}

	@Override
	public Movie getMovieById(Integer id) {
		return (Movie) getCurrentSession().get(
				Movie.class, id);
	}

	@Override
	public void removeMovie(Integer id) {
		Movie movie = getMovieById(id);
		if (null != movie) {
			getCurrentSession().delete(movie);
		}

	}
}
