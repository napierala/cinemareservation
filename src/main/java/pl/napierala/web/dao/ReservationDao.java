package pl.napierala.web.dao;

import java.util.List;

import pl.napierala.web.model.Reservation;

public interface ReservationDao {
	
    public void addReservation(Reservation reservation);
    public void updateReservation(Reservation reservation);
    public List<Reservation> getReservationList();
    public Reservation getReservationById(Integer id);
    public void removeReservation(Integer id);
	
}
