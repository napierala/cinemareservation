package pl.napierala.web.dao;

import java.util.List;

import pl.napierala.web.model.MovieShowing;

public interface MovieShowingDao {
	
    public void addMovieShowing(MovieShowing movie);
    public void updateMovieShowing(MovieShowing movie);
    public List<MovieShowing> getMovieShowingList();
    public MovieShowing getMovieShowingById(Integer id);
    public void removeMovieShowing(Integer id);
	
    public List<MovieShowing> getTodaysMovieShowings();
}
