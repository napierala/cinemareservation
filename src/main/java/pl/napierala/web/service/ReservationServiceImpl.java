package pl.napierala.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.napierala.web.dao.ReservationDao;
import pl.napierala.web.model.Reservation;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

	@Autowired
	private ReservationDao reservationDao;
	
	@Override
	public void addReservation(Reservation reservation) {
		this.reservationDao.addReservation(reservation);
	}

	@Override
	public void updateReservation(Reservation reservation) {
		this.reservationDao.updateReservation(reservation);
	}

	@Override
	public List<Reservation> getReservationList() {
		return this.reservationDao.getReservationList();
	}

	@Override
	public Reservation getReservationById(Integer id) {
		return this.reservationDao.getReservationById(id);
	}

	@Override
	public void removeReservation(Integer id) {
		this.reservationDao.removeReservation(id);

	}
}
