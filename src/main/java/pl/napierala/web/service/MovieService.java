package pl.napierala.web.service;

import java.util.List;

import pl.napierala.web.model.Movie;

public interface MovieService {

	public void addMovie(Movie movie);
    public void updateMovie(Movie movie);
    public List<Movie> getMovieList();
    public Movie getMovieById(Integer id);
    public void removeMovie(Integer id);
	
}
