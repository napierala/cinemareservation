package pl.napierala.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.napierala.web.dao.MovieShowingDao;
import pl.napierala.web.model.MovieShowing;

@Service
@Transactional
public class MovieShowingServiceImpl implements MovieShowingService {

	@Autowired
	private MovieShowingDao movieShowingDao;

	@Override
	public void addMovieShowing(MovieShowing movieShowing) {
		this.movieShowingDao.addMovieShowing(movieShowing);
	}

	@Override
	public void updateMovieShowing(MovieShowing movieShowing) {
		this.movieShowingDao.updateMovieShowing(movieShowing);
	}

	@Override
	public List<MovieShowing> getMovieShowingList() {
		return this.movieShowingDao.getMovieShowingList();
	}

	@Override
	public MovieShowing getMovieShowingById(Integer id) {
		return this.movieShowingDao.getMovieShowingById(id);
	}

	@Override
	public void removeMovieShowing(Integer id) {
		this.movieShowingDao.removeMovieShowing(id);

	}

	@Override
	public List<MovieShowing> getTodaysMovieShowings() {
		return this.movieShowingDao.getTodaysMovieShowings();
	}
}
