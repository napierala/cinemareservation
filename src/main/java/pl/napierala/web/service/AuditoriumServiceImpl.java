package pl.napierala.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.napierala.web.dao.AuditoriumDao;
import pl.napierala.web.model.Auditorium;

@Service
@Transactional
public class AuditoriumServiceImpl implements AuditoriumService {

	@Autowired
	private AuditoriumDao auditoriumDao;
	
	@Override
	public void addAuditorium(Auditorium auditorium) {
		this.auditoriumDao.addAuditorium(auditorium);
	}

	@Override
	public void updateAuditorium(Auditorium auditorium) {
		this.auditoriumDao.updateAuditorium(auditorium);
	}

	@Override
	public List<Auditorium> getAuditoriumList() {
		return this.auditoriumDao.getAuditoriumList();
	}

	@Override
	public Auditorium getAuditoriumById(Integer id) {
		return this.auditoriumDao.getAuditoriumById(id);
	}

	@Override
	public void removeAuditorium(Integer id) {
		this.auditoriumDao.removeAuditorium(id);

	}
}
