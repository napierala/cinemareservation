package pl.napierala.web.service;

import java.util.List;

import pl.napierala.web.model.MovieShowing;

public interface MovieShowingService {

	public void addMovieShowing(MovieShowing movie);
    public void updateMovieShowing(MovieShowing movie);
    public List<MovieShowing> getMovieShowingList();
    public MovieShowing getMovieShowingById(Integer id);
    public void removeMovieShowing(Integer id);
    
    public List<MovieShowing> getTodaysMovieShowings();
	
}
