package pl.napierala.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.napierala.web.dao.MovieDao;
import pl.napierala.web.model.Movie;

@Service
@Transactional
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieDao movieDao;
	
	@Override
	public void addMovie(Movie movie) {
		this.movieDao.addMovie(movie);
	}

	@Override
	public void updateMovie(Movie movie) {
		this.movieDao.updateMovie(movie);
	}

	@Override
	public List<Movie> getMovieList() {
		return this.movieDao.getMovieList();
	}

	@Override
	public Movie getMovieById(Integer id) {
		return this.movieDao.getMovieById(id);
	}

	@Override
	public void removeMovie(Integer id) {
		this.movieDao.removeMovie(id);

	}
}
