package pl.napierala.web.service;

import java.util.List;

import pl.napierala.web.model.Auditorium;

public interface AuditoriumService {

	public void addAuditorium(Auditorium auditorium);

	public void updateAuditorium(Auditorium auditorium);

	public List<Auditorium> getAuditoriumList();

	public Auditorium getAuditoriumById(Integer id);

	public void removeAuditorium(Integer id);

}
