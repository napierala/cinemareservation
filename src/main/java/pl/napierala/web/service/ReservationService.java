package pl.napierala.web.service;

import java.util.List;

import pl.napierala.web.model.Reservation;

public interface ReservationService {

	public void addReservation(Reservation movie);
    public void updateReservation(Reservation movie);
    public List<Reservation> getReservationList();
    public Reservation getReservationById(Integer id);
    public void removeReservation(Integer id);
	
}
