package pl.napierala.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "movies", uniqueConstraints = @UniqueConstraint(columnNames = { "title" }))
public class Movie implements Comparable<Movie> {

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = 0;

	@Column(name = "title", nullable = false, length = 100)
	private String title;

	@Column(name = "duration", nullable = false)
	private Integer duration;

	@Column(name = "description", nullable = false, length = 2048)
	private String description;

	public Movie() {
	}

	public Movie(String title, Integer duration, String description) {
		this.title = title;
		this.duration = duration;
		this.description = description;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", title=" + title + ", duration="
				+ duration + ", description=" + description + "]";
	}

	@Override
	public int compareTo(Movie movie) {

		if (movie.getId() == this.getId()) {
			return 0;
		} else if (movie.getId() > this.getId()) {
			return 1;
		} else {
			return -1;
		}

	}
}
