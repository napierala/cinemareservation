package pl.napierala.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "auditoriums", uniqueConstraints = @UniqueConstraint(columnNames = { "room_number"}))
public class Auditorium {

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = 0;
	
	@Column(name = "room_number", nullable = false, length = 20)
	private String roomNumber;
	
	@Column(name = "no_of_seats", nullable = false)
	private Integer noOfSeats;

	public Auditorium() {
	}

	public Auditorium(String roomNumber, Integer noOfSeats) {
		this.roomNumber = roomNumber;
		this.noOfSeats = noOfSeats;
	}

	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoomNumber() {
		return this.roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public Integer getNoOfSeats() {
		return this.noOfSeats;
	}

	public void setNoOfSeats(Integer noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	@Override
	public String toString() {
		return "Auditorium [id=" + id + ", roomNumber=" + roomNumber
				+ ", noOfSeats=" + noOfSeats + "]";
	}

}

