package pl.napierala.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "reservations")
public class Reservation {

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Email
	@Column(name = "email", nullable = false, length = 255)
	private String email;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "movie_showing_id", nullable = false)
	private MovieShowing movieShowing;
	
	@Min(1)
	@Column(name = "seats_reserved", nullable = false)
	private Integer seatsReserved;

	public Reservation() {
	}

	public Reservation(String email, MovieShowing movieShowing,
			Integer seatsReserved) {

		this.email = email;
		this.movieShowing = movieShowing;
		this.seatsReserved = seatsReserved;
	}
	
	public Reservation(MovieShowing movieShowing){
		this.movieShowing = movieShowing;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public MovieShowing getMovieShowing() {
		return this.movieShowing;
	}

	public void setMovieShowing(MovieShowing movieShowing) {
		this.movieShowing = movieShowing;
	}

	public Integer getSeatsReserved() {
		return this.seatsReserved;
	}

	public void setSeatsReserved(Integer seatsReserved) {
		this.seatsReserved = seatsReserved;
	}

}
