package pl.napierala.web.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "movie_showings", uniqueConstraints = @UniqueConstraint(columnNames = {
		"movie_id", "auditorium_id", "show_time" }))
public class MovieShowing implements Comparable<MovieShowing> {

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = 0;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "movie_id", nullable = false)
	private Movie movie;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "auditorium_id", nullable = false)
	private Auditorium auditorium;
	
	@Column(name = "show_time", nullable = false)
	private Timestamp showTime;
	
	@Column(name = "seats_left", nullable = false)
	private Integer seatsLeft;

	public MovieShowing() {
	}

	public MovieShowing(Movie movie, Auditorium auditorium, Timestamp showTime,
			Integer seatsLeft) {
		this.movie = movie;
		this.auditorium = auditorium;
		this.showTime = showTime;
		this.seatsLeft = seatsLeft;
	}
	
	public MovieShowing(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Movie getMovie() {
		return this.movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	public Auditorium getAuditorium() {
		return this.auditorium;
	}

	public void setAuditorium(Auditorium auditorium) {
		this.auditorium = auditorium;
	}
	
	public Timestamp getShowTime() {
		return this.showTime;
	}

	public void setShowTime(Timestamp showTime) {
		this.showTime = showTime;
	}
	
	public Integer getSeatsLeft() {
		return this.seatsLeft;
	}

	public void setSeatsLeft(Integer seatsLeft) {
		this.seatsLeft = seatsLeft;
	}

	@Override
	public String toString() {
		return "MovieShowing [id=" + id + ", movie=" + movie + ", auditorium="
				+ auditorium + ", showTime=" + showTime + ", seatsLeft="
				+ seatsLeft + "]";
	}
	

	@Override
	public int compareTo(MovieShowing movieShowing) {

		if (movieShowing.getId() == this.getId()) {
			return 0;
		} else if (movieShowing.getId() > this.getId()) {
			return 1;
		} else {
			return -1;
		}

	}

}
