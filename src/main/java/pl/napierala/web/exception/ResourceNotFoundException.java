package pl.napierala.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="Zasób nie istnieje.")
public class ResourceNotFoundException extends RuntimeException {


	private static final long serialVersionUID = 5044483217669454230L;
         
}
