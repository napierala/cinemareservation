package pl.napierala.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pl.napierala.web.service.ReservationService;


@Controller
@RequestMapping(value = "/admin/reservation")
public class AdminReservationController {
	
	@Autowired
	private ReservationService reservationService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listReservations() {

		ModelAndView model = new ModelAndView("adminReservation");

		model.addObject("list", reservationService.getReservationList());

		return model;

	}

}
