package pl.napierala.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import pl.napierala.web.model.MovieShowing;
import pl.napierala.web.service.AuditoriumService;
import pl.napierala.web.service.MovieService;
import pl.napierala.web.service.MovieShowingService;

@SessionAttributes("movieShowingEntity")
@Controller
@RequestMapping(value = "/admin/movieShowing")
public class AdminMovieShowingController {

	private final Logger logger = LoggerFactory
			.getLogger(AdminMovieShowingController.class);

	@Autowired
	private MovieShowingService movieShowingService;

	@Autowired
	private MovieService movieService;

	@Autowired
	private AuditoriumService auditoriumService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listMovieShowings(
			@RequestParam(value = "result", required = false) String result,
			@RequestParam(value = "error", required = false) String error) {

		ModelAndView model = new ModelAndView("adminMovieShowing",
				"movieShowingEntity", new MovieShowing());

		model.addObject("movieShowingList",
				movieShowingService.getMovieShowingList());
		model.addObject("movieList", movieService.getMovieList());
		model.addObject("auditoriumList", auditoriumService.getAuditoriumList());

		model.addObject("result", result);
		model.addObject("error", error);

		return model;

	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMovieShowing(
			@ModelAttribute("movieShowingEntity") MovieShowing movieShowing,
			BindingResult result, ModelMap model) {

		logger.info(movieShowing.toString());

		if (movieShowing.getId() == 0) {
			movieShowingService.addMovieShowing(movieShowing);
		} else {
			movieShowingService.updateMovieShowing(movieShowing);
		}

		String resultStr;

		if (result.hasErrors()) {
			resultStr = "There is a error with the movieShowing.";
		} else {
			resultStr = "The movieShowing has been persisted.";
		}

		model.addAttribute("result", resultStr);

		return "redirect:/admin/movieShowing/";

	}

	@RequestMapping("/remove/{id}")
	public String removeMovieShowing(@PathVariable("id") int id, ModelMap model) {

		try {

			this.movieShowingService.removeMovieShowing(id);

			model.addAttribute("result", "MovieShowing deleted.");

		} catch (Throwable ex) {

			model.addAttribute("error",
					"Cannot delete this MovieShowing as it has dependencies.");

		}

		return "redirect:/admin/movieShowing/";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editMovieShowing(@PathVariable("id") int id) {

		ModelAndView model = new ModelAndView("adminMovieShowing",
				"movieShowingEntity",
				movieShowingService.getMovieShowingById(id));

		model.addObject("movieShowingList",
				movieShowingService.getMovieShowingList());
		model.addObject("movieList", movieService.getMovieList());
		model.addObject("auditoriumList", auditoriumService.getAuditoriumList());

		return model;

	}
}
