package pl.napierala.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.napierala.web.model.Auditorium;
import pl.napierala.web.service.AuditoriumService;

@Controller
@RequestMapping(value = "/admin/auditorium")
public class AdminAuditoriumController {

	private final Logger logger = LoggerFactory
			.getLogger(AdminAuditoriumController.class);

	@Autowired
	private AuditoriumService auditoriumService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listMovies(
			@RequestParam(value = "result", required = false) String result,
			@RequestParam(value = "error", required = false) String error) {

		ModelAndView model = new ModelAndView("adminAuditorium",
				"auditoriumEntity", new Auditorium());

		model.addObject("auditoriumList", auditoriumService.getAuditoriumList());
		model.addObject("result", result);
		model.addObject("error", error);

		return model;

	}

	// For add and update person both
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMovie(
			@ModelAttribute("auditoriumEntity") Auditorium auditorium,
			BindingResult result, ModelMap model) {

		if (auditorium.getId() == 0) {
			auditoriumService.addAuditorium(auditorium);
		} else {
			auditoriumService.updateAuditorium(auditorium);
		}

		String resultStr;

		if (result.hasErrors()) {
			resultStr = "There is a error with the auditorium.";
		} else {
			resultStr = "The auditorium has been persisted.";
		}

		model.addAttribute("result", resultStr);

		return "redirect:/admin/auditorium/";

	}

	@RequestMapping("/remove/{id}")
	public String removePerson(@PathVariable("id") int id, ModelMap model) {

		try {
			this.auditoriumService.removeAuditorium(id);

			model.addAttribute("result", "Auditorium deleted.");

		} catch (Throwable ex) {

			model.addAttribute("error",
					"Cannot delete this MovieShowing as it has dependencies.");

		}
		return "redirect:/admin/auditorium/";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editMovie(@PathVariable("id") int id) {

		ModelAndView model = new ModelAndView("adminAuditorium",
				"auditoriumEntity", auditoriumService.getAuditoriumById(id));

		model.addObject("auditoriumList", auditoriumService.getAuditoriumList());

		return model;

	}
}
