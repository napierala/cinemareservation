package pl.napierala.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import pl.napierala.web.exception.ResourceNotFoundException;
import pl.napierala.web.model.Movie;
import pl.napierala.web.service.MovieService;

@Controller
@RequestMapping(value = "/movie")
public class MovieController {

	@Autowired
	private MovieService movieService;

	@RequestMapping("/{movieID}")
	public ModelAndView showMovie(@PathVariable Integer movieID) {

		// ModelAndView modelAndView = new ModelAndView("list-of-teams");
		//
		// List<Team> teams = teamService.getTeams();
		// modelAndView.addObject("teams", teams);
		//
		// return modelAndView;

		ModelAndView model = new ModelAndView("showMovie");

		Movie movie = movieService.getMovieById(movieID);

		if (movie == null) {
			throw new ResourceNotFoundException();
		}
		// Get the movie from the DB.
		model.addObject("movie", movie);

		return model;

	}

}
