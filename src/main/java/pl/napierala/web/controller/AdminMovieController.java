package pl.napierala.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pl.napierala.web.model.Movie;
import pl.napierala.web.service.MovieService;

@Controller
@RequestMapping(value = "/admin/movie")
public class AdminMovieController {

	private final Logger logger = LoggerFactory.getLogger(AdminMovieController.class);
	
	@Autowired
	private MovieService movieService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView listMovies(@RequestParam(value = "result", required = false) String result) {

		ModelAndView model = new ModelAndView("adminMovie", "movieEntity",
				new Movie());

		model.addObject("movieList", movieService.getMovieList());
		model.addObject("result", result);

		return model;

	}

	// For add and update person both
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMovie(@ModelAttribute("movieEntity") Movie movie,
			BindingResult result, ModelMap model) {

		logger.info(movie.toString());
		
		if (movie.getId() == 0) {
			movieService.addMovie(movie);
		} else {
			movieService.updateMovie(movie);
		}

		String resultStr;

		if (result.hasErrors()) {
			resultStr = "There is a error with the movie.";
		} else {
			resultStr = "The movie has been persisted.";
		}

		model.addAttribute("result", resultStr);

		return "redirect:/admin/movie/";

	}

	@RequestMapping("/remove/{id}")
	public String removePerson(@PathVariable("id") int id,  ModelMap model) {

		this.movieService.removeMovie(id);
		
		model.addAttribute("result", "Movie deleted.");
		
		return "redirect:/admin/movie/";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editMovie(@PathVariable("id") int id) {

		ModelAndView model = new ModelAndView("adminMovie", "movieEntity",
				movieService.getMovieById(id));

		model.addObject("movieList", movieService.getMovieList());

		return model;

	}
}
