package pl.napierala.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;

import pl.napierala.web.model.MovieShowing;
import pl.napierala.web.model.Movie;
import pl.napierala.web.service.MovieShowingService;

@Controller
public class MainController {

	@Autowired
	private MovieShowingService movieShowingService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView mainPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Kino - booking of movie tickets !");
		model.addObject("message", "Best movies this side of the coast!");
		model.setViewName("main");

		// Get the movie list from the DB.
		List<MovieShowing> movieShowingList = movieShowingService
				.getTodaysMovieShowings();

		// Group by the movie showings based on the movie.
		Multimap<Movie, MovieShowing> movieShowingsGroupedByMovies = TreeMultimap.create();

		for (MovieShowing movieShowing : movieShowingList) {
			movieShowingsGroupedByMovies.put(movieShowing.getMovie(),
					movieShowing);
		}

		// for (Movie key : movieShowingsGroupedByMovies.keySet()) {
		//
		// Collection<MovieShowing> movieShowingsForThisMovie =
		// movieShowingsGroupedByMovies
		// .get(key);
		//
		// }

		//Add the grouped movie showings as a standard map.
		model.addObject("movieShowingsGroupedByMovies",
				movieShowingsGroupedByMovies.asMap());
		return model;
	}

}