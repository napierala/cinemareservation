package pl.napierala.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import pl.napierala.web.model.MovieShowing;
import pl.napierala.web.model.Reservation;
import pl.napierala.web.service.ReservationService;

@SessionAttributes("reservationEntity")
@Controller
@RequestMapping(value = "/reservation")
public class ReservationController {

	private final Logger logger = LoggerFactory
			.getLogger(ReservationController.class);

	@Autowired
	private ReservationService reservationService;

	@Autowired
	private Validator validator;

	@RequestMapping(value = "/form/{movieShowingID}", method = RequestMethod.GET)
	public ModelAndView showReservationForm(@PathVariable Integer movieShowingID) {

		ModelAndView model = new ModelAndView("reservationForm",
				"reservationEntity", new Reservation(new MovieShowing(
						movieShowingID)));

		model.addObject("title", "Movie ticket booking");

		return model;
	}

	@RequestMapping(value = "/make", method = RequestMethod.POST)
	public String makeReservation(
			@Valid @ModelAttribute("reservationEntity") Reservation reservation,
			BindingResult result, ModelMap model) {

		try {
			logger.info("MovieShowingID:"
					+ reservation.getMovieShowing().getId());

			reservationService.addReservation(reservation);

			if (result.hasErrors()) {
				model.addAttribute("error", "There is a problem with the booking");

			} else {
				model.addAttribute("result", "The booking is succesfull.");
			}


		} catch (Throwable ex) {

			//Loop to get the root cause
			while (ex.getCause() != null) {
				ex = ex.getCause();
			}

			model.addAttribute("error", ex.getMessage());

		}
		// Pass the movieShowingID
		model.addAttribute("reservationID", reservation.getId());
		

		return "reservationResult";
	}

}