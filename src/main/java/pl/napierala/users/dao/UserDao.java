package pl.napierala.users.dao;

import pl.napierala.users.model.User;

public interface UserDao {

	User findByUserName(String username);

}