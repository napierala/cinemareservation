<%@tag language="java" description="The web page layout"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@attribute name="header" fragment="true"%>
<%@attribute name="footer" fragment="true"%>

<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}
</style>

<html>
<body>
	<div id="pageheader">

		<jsp:invoke fragment="header" />

		<sec:authorize access="isAuthenticated()">
			|
			<a href="/Cinema/logout"> Logout</a>
		</sec:authorize>


	</div>
	<div id="body">
		<c:if test="${not empty result}">
			<div class="msg">${result}</div>
		</c:if>
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<jsp:doBody />
	</div>
	<div id="pagefooter">
		<jsp:invoke fragment="footer" />
	</div>
</body>
</html>