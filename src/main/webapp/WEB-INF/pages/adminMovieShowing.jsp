<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<title>${movie.title}</title>
      	<a href="/Cinema/admin"> Admin </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
 		
		<form:form method="POST" modelAttribute="movieShowingEntity"  action="${pageContext.request.contextPath}/admin/movieShowing/add" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<table>
			<c:if test="${movieShowingEntity.getId() != 0}">
	    		<tr>
	        		<td>
	            		<form:label path="id">
	                		<spring:message text="ID"/>
	            		</form:label>
	        		</td>
	        		<td>
	            		<form:input path="id" readonly="true" size="8"  disabled="true" />
	            		<form:hidden path="id" />
	        		</td> 
	    		</tr>
    		</c:if>
    		<c:if test="${movieShowingEntity.getId() == 0}">
	    		<tr>
	        		<td>
	            		<form:label path="movie.id">
	                		<spring:message text="Movie"/>
	            		</form:label>
	        		</td>
	        		<td>
	        			<form:select path="movie.id">
							<form:options items="${movieList}" itemValue="id" itemLabel="title"/>
						</form:select>
	        		</td> 
	    		</tr>
	    		<tr>
	        		<td>
	            		<form:label path="auditorium.id">
	                		<spring:message text="Auditorium"/>
	            		</form:label>
	        		</td>
	        		<td>
	        			<form:select path="auditorium.id">
							<form:options items="${auditoriumList}" itemValue="id" itemLabel="roomNumber"/>
						</form:select>
	        		</td> 
	    		</tr>
    		</c:if>
		    <tr>
		        <td>
		            <form:label path="showTime">
		                <spring:message text="The ShowTime:" />
		            </form:label>
		        </td>
		        <td>
		        	<font color='red'><form:errors path='showTime' /></font>
		            <form:input type="datetime-local" path="showTime" />
		        </td> 
		    </tr>
		    <tr>
		        <td colspan="2">
		            <c:if test="${movieShowingEntity.getId() != 0}">
		                <input type="submit"
										value="<spring:message text="Edit"/>" />
		            </c:if>
		            <c:if test="${movieShowingEntity.getId() == 0}">
		                <input type="submit"
										value="<spring:message text="Add"/>" />
		            </c:if>
		        </td>
		    </tr>
		</table>  
		</form:form>
		<br>
		<h3>MovieShowing list:</h3>
		<c:if test="${!empty movieShowingList}">
		    <table class="tg">
		    <tr>
		        <th width="80">ID</th>
		        <th width="200">Show Time</th>
		        <th width="100">Seats Left</th>
		        <th width="100">Movie</th>
		        <th width="100">Auditorium</th>
		        <th width="60"></th>
		        <th width="60"></th>
		    </tr>
		    <c:forEach items="${movieShowingList}" var="movieShowing">
		        <tr>
		            <td>${movieShowing.getId()}</td>
		            <td>${movieShowing.showTime}</td>
		            <td>${movieShowing.seatsLeft}</td>
		            <td>${movieShowing.movie.title}</td>
		            <td>${movieShowing.auditorium.roomNumber}</td>
		            <td><a href="<c:url value='/admin/movieShowing/edit/${movieShowing.getId()}' />">Edit</a></td>
		            <td><a href="<c:url value='/admin/movieShowing/remove/${movieShowing.getId()}'/>" onclick="return confirm('Are you sure you want to delete this MovieShowing?')" >Remove</a></td>
		        </tr>
		    </c:forEach>
		    </table>
		</c:if>
    </jsp:body>
</t:genericpage>




