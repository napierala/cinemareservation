<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<h1>${title}</h1>
      	
      	<a href="/Cinema/admin"> Admin </a>
      	
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
        <form:form method="POST" modelAttribute="reservationEntity" action="${pageContext.request.contextPath}/reservation/make">
		<table>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			
				
				<tr>
					<td><form:label path="email">Email:</form:label></td>
					<font color='red'><form:errors path='email' /></font>
					<td><input type="email" id="email" name="email"/></td>
				</tr>

				<tr>
					<td><form:label path="seatsReserved">Number of seats:</form:label></td>
					<font color='red'><form:errors path='seatsReserved' /></font>
					<td><input type="number" id="seatsReserved" name="seatsReserved" min="1"/></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" value="Book" onclick="return confirm('Are you sure you want to book this ticket?')" /></td>
				</tr>
		</table>
	</form:form>
    </jsp:body>
</t:genericpage>