<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<title>${title}</title>
      	
      	<a href="/Cinema/"> Cinema </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
		<br>
		<a href="${pageContext.request.contextPath}/admin/movie/"> Movies </a>
		<br>
		<a href="${pageContext.request.contextPath}/admin/auditorium/"> Auditoriums </a>
		<br>
		<a href="${pageContext.request.contextPath}/admin/reservation/"> Reservations </a>
		<br>
		<a href="${pageContext.request.contextPath}/admin/movieShowing/"> MovieShowings </a>
      
    </jsp:body>
</t:genericpage>