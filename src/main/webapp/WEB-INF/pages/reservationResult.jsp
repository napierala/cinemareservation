<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
		<a href="/Cinema/admin"> Admin </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>

		<c:if test="${not empty reservationID}">
	
			The booking number: ${reservationID}
		
		</c:if>
    </jsp:body>
</t:genericpage>
