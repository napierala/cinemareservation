<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<title>${movie.title}</title>
      	<a href="/Cinema/admin"> Admin </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
        <h3>${movie.title}:</h3>
		<br> Movie duration: ${movie.duration}
		<br> Description: ${movie.description}
		<br>
    </jsp:body>
</t:genericpage>




