<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
	
		<a href="/Cinema/admin"> Admin </a>
		
    </jsp:attribute>
	<jsp:attribute name="footer">
      <p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
		<h1>${title}</h1>
	  	<h2>${message}</h2>
	  
        <c:if test="${not empty movieShowingsGroupedByMovies}">

		<ul>
			<c:url var="movieLink" value="/movie/" />
			<c:url var="reservationLink" value="/reservation/form/" />
			
			<!-- A Map<Movie, Collection<MovieShowing>> -->
			<c:forEach var="movieShowingsForAMovie"
					items="${movieShowingsGroupedByMovies}">

				
				<li>
					<c:set var="movie" value="${movieShowingsForAMovie.key}" />

					<a href="${movieLink}${movie.getId()}">${movie.title}</a>
					<ul>
						<!-- a Collection<MovieShowing> -->
						<c:set var="movieShowings" value="${movieShowingsForAMovie.value}" />
						<c:forEach var="movieShowing" items="${movieShowings}">
							<li>
								<a href="${reservationLink}${movieShowing.getId()}">
									<fmt:formatDate type="time" value="${movieShowing.showTime}" />
								</a>
							</li>
						</c:forEach>
					</ul>
				</li>
			</c:forEach>

		</ul>
		
		<!-- Sort the movie list -->
		
	</c:if>
	<c:if test="${empty movieShowingsGroupedByMovies}">
	
		Unfortunately the cinema is closed today!
		
	</c:if>
    </jsp:body>
</t:genericpage>