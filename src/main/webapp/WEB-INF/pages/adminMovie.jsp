<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<title>${movie.title}</title>
      	<a href="/Cinema/admin"> Admin </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
		
		<form:form method="POST" modelAttribute="movieEntity"  action="${pageContext.request.contextPath}/admin/movie/add" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<table>
			<c:if test="${movieEntity.getId() != 0}">
	    		<tr>
	        		<td>
	            		<form:label path="id">
	                		<spring:message text="ID"/>
	            		</form:label>
	        		</td>
	        		<td>
	            		<form:input path="id" readonly="true" size="8"  disabled="true" />
	            		<form:hidden path="id" />
	        		</td> 
	    		</tr>
    		</c:if>
		    <tr>
		        <td>
		            <form:label path="title">
		                <spring:message text="Title:" />
		            </form:label>
		        </td>
		        <td>
		        	<font color='red'><form:errors path='title' /></font>
		            <form:input path="title" />
		        </td> 
		    </tr>
		    <tr>
		        <td>
		            <form:label path="duration">
		                <spring:message text="Duration:" />
		            </form:label>
		        </td>
		        <td>
		        	<font color='red'><form:errors path='duration' /></font>
		            <form:input path="duration" />
		        </td>
		    </tr>
		     <tr>
		        <td>
		            <form:label path="description">
		                <spring:message text="Description:" />
		            </form:label>
		        </td>
		        <td>
		        	<font color='red'><form:errors path='description' /></font>
		            <form:input path="description" />
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <c:if test="${movieEntity.getId() != 0}">
		                <input type="submit"
										value="<spring:message text="Edit"/>" />
		            </c:if>
		            <c:if test="${movieEntity.getId() == 0}">
		                <input type="submit"
										value="<spring:message text="Add"/>" />
		            </c:if>
		        </td>
		    </tr>
		</table>  
		</form:form>
		<br>
		<h3>Movie list:</h3>
		<c:if test="${!empty movieList}">
		    <table class="tg">
		    <tr>
		        <th width="80">ID</th>
		        <th width="120">Title</th>
		        <th width="120">Duration</th>
		        <th width="200">Description</th>
		        <th width="60"></th>
		        <th width="60"></th>
		    </tr>
		    <c:forEach items="${movieList}" var="movie">
		        <tr>
		            <td>${movie.getId()}</td>
		            <td>${movie.title}</td>
		            <td>${movie.duration}</td>
		            <td>${movie.description}</td>
		            <td><a href="<c:url value='/admin/movie/edit/${movie.getId()}' />">Edit</a></td>
		            <td><a href="<c:url value='/admin/movie/remove/${movie.getId()}' />" onclick="return confirm('Are you sure you want to delete this movie?')">Remove</a></td>
		        </tr>
		    </c:forEach>
		    </table>
		</c:if>
    </jsp:body>
</t:genericpage>




