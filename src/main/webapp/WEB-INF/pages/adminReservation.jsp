<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<title>${movie.title}</title>
      	<a href="/Cinema/admin"> Admin </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
 		
		<h3>Reservation list:</h3>
		<c:if test="${!empty list}">
		    <table class="tg">
		    <tr>
		        <th width="20">ID</th>
		        <th width="100">Email</th>
		        <th width="100">MovieShowing</th>
		        <th width="100">Seats reserved</th>
		    </tr>
		    <c:forEach items="${list}" var="reservation">
		        <tr>
		            <td>${reservation.getId()} |</td>
		            <td>${reservation.email} |</td>
		            <td>${reservation.movieShowing.showTime} @ ${reservation.movieShowing.auditorium.roomNumber} |</td>
		            <td>${reservation.seatsReserved}</td>
		
		        </tr>
		    </c:forEach>
		    </table>
		</c:if>
    </jsp:body>
</t:genericpage>




