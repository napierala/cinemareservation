<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage>
	<jsp:attribute name="header">
      	<title>${movie.title}</title>
      	<a href="/Cinema/admin"> Admin </a>
    </jsp:attribute>
	<jsp:attribute name="footer">
      	<p id="copyright">Copyright Cinema Company.</p>
    </jsp:attribute>
	<jsp:body>
		
		<form:form method="POST" modelAttribute="auditoriumEntity"  action="${pageContext.request.contextPath}/admin/auditorium/add" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<table>
			<c:if test="${auditoriumEntity.getId() != 0}">
	    		<tr>
	        		<td>
	            		<form:label path="id">
	                		<spring:message text="ID"/>
	            		</form:label>
	        		</td>
	        		<td>
	            		<form:input path="id" readonly="true" size="8"  disabled="true" />
	            		<form:hidden path="id" />
	        		</td> 
	    		</tr>
    		</c:if>
		    <tr>
		        <td>
		            <form:label path="roomNumber">
		                <spring:message text="Room Number:" />
		            </form:label>
		        </td>
		        <td>
		        	<font color='red'><form:errors path='roomNumber' /></font>
		            <form:input path="roomNumber" />
		        </td> 
		    </tr>
		    <c:if test="${auditoriumEntity.getId() == 0}">
		    <tr>
		        <td>
		            <form:label path="noOfSeats">
		                <spring:message text="Number of seats:" />
		            </form:label>
		        </td>
		        <td>
		        	<font color='red'><form:errors path='noOfSeats' /></font>
		            <form:input path="noOfSeats" />
		        </td>
		    </tr>
		   </c:if>
		    <tr>
		        <td colspan="2">
		            <c:if test="${auditoriumEntity.getId() != 0}">
		                <input type="submit"
										value="<spring:message text="Edit"/>" />
		            </c:if>
		            <c:if test="${auditoriumEntity.getId() == 0}">
		                <input type="submit"
										value="<spring:message text="Add"/>" />
		            </c:if>
		        </td>
		    </tr>
		</table>  
		</form:form>
		<br>
		<h3>Auditorium list:</h3>
		<c:if test="${!empty auditoriumList}">
		    <table class="tg">
		    <tr>
		        <th width="80">ID</th>
		        <th width="120">Room number</th>
		        <th width="120">Number of seats</th>
		        <th width="60"></th>
		        <th width="60"></th>
		    </tr>
		    <c:forEach items="${auditoriumList}" var="auditorium">
		        <tr>
		            <td>${auditorium.getId()}</td>
		            <td>${auditorium.roomNumber}</td>
		            <td>${auditorium.noOfSeats}</td>
		            <td><a href="<c:url value='/admin/auditorium/edit/${auditorium.getId()}' />">Edit</a></td>
		            <td><a href="<c:url value='/admin/auditorium/remove/${auditorium.getId()}' />" onclick="return confirm('Are you sure you want to delete this Auditorium?')" >Remove</a></td>
		        </tr>
		    </c:forEach>
		    </table>
		</c:if>
    </jsp:body>
</t:genericpage>




