CREATE TABLE movies (
	id int NOT NULL AUTO_INCREMENT,
	title VARCHAR(100) NOT NULL, 				-- The title of the movie.
	duration int NOT NULL, 						-- Duration of the movie in minutes.
	description VARCHAR(2048) NOT NULL,			-- The description of the movie.
	PRIMARY KEY (id),
	UNIQUE KEY uni_title (title),				-- The title needs to be unique.
	CHECK (duration > 0)						-- The duration needs to be more at least 1.
)  ENGINE=INNODB;

CREATE TABLE auditoriums (
	id int NOT NULL AUTO_INCREMENT,		
	room_number VARCHAR(20) NOT NULL,			-- The room number of the movie e.g. 26.
	no_of_seats int NOT NULL,					-- The number of seats in the auditorium e.g. 125.
	PRIMARY KEY (id),
	UNIQUE KEY uni_room_number (room_number),	-- The room number needs to be unique.
	CHECK (no_of_seats > 0)						-- The number of seats needs to be more at least 1.
) ENGINE=INNODB;
  
CREATE TABLE movie_showings (
	id int NOT NULL AUTO_INCREMENT,
	movie_id int NOT NULL,						-- The id of the movie showing.
	auditorium_id int NOT NULL,					-- The id of the auditorium where the movie is showing.
	show_time TIMESTAMP NOT NULL, 				-- The show time of the movie.
	seats_left int,								/*The number of seats still left. 
												 At insert will be set to the max seats in the auditorium. 
												 by a trigger.*/
	PRIMARY KEY (id),
	CONSTRAINT fk_movie FOREIGN KEY (movie_id) REFERENCES movies (id),
	CONSTRAINT fk_auditorium FOREIGN KEY (auditorium_id) REFERENCES auditoriums (id),
	UNIQUE KEY uni_movie_audi_st (movie_id, auditorium_id, show_time),	
	
	CHECK (seats_left >= 0)						-- The numer of seats left in this showing.
	
) ENGINE=INNODB;

/**
 * A trigger to implement the seats_left check in MySQL.
 */
CREATE DEFINER=`root`@`localhost` TRIGGER `db`.`movie_showings_BEFORE_UPDATE` 
BEFORE UPDATE ON `movie_showings` 
FOR EACH ROW
	BEGIN
     DECLARE msg varchar(255);
     IF (NEW.seats_left<0)
     THEN
        SET msg = 'No seats left for this movie showing.';
        SIGNAL sqlstate '45000' SET message_text = msg;
     END IF; 
     END

/**
 * A handy trigger to set the initial value for 
 * the seats_left from the  actual auditorium.
 */
CREATE DEFINER=`root`@`localhost` TRIGGER `db`.`movie_showings_BEFORE_INSERT` 
BEFORE INSERT ON `movie_showings` 
FOR EACH ROW
	BEGIN
	SET NEW.seats_left = (Select no_of_seats from auditoriums
	where auditoriums.id = new.auditorium_id);
	END

CREATE TABLE reservations (
	id int NOT NULL AUTO_INCREMENT,
	email VARCHAR(255) NOT NULL,				-- The email is required for a reservation.
	
	movie_showing_id int NOT NULL,				-- The id of the movie_showing that that is reserved.
	seats_reserved int NOT NULL,				-- The number of seats reserved.
	
    PRIMARY KEY (id),
	CONSTRAINT fk_movie_showing FOREIGN KEY (movie_showing_id) REFERENCES movie_showings (id),
	
	CHECK (seats_reserved > 0)					-- Seats reserverd need to be at least 1.
	
) ENGINE=INNODB;

/**
 * The trigger to take care of seats_reserved > 0 
 * and to update the seats_left on the movie_showing.
 */
CREATE DEFINER=`root`@`localhost` TRIGGER 
`db`.`reservations_BEFORE_INSERT` 
BEFORE INSERT ON `reservations` 
FOR EACH ROW
	BEGIN
		IF (NEW.seats_reserved < 0)
			THEN
				SIGNAL sqlstate '45000' 
                SET message_text = 'At least one seat needs to be reserverd';
        END IF;
        
        update movie_showings 
        set seats_left = seats_left - New.seats_reserved 
        where id = New.movie_showing_id;
	END
