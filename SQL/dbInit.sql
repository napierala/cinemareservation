CREATE  TABLE users (
  username VARCHAR(50) NOT NULL ,
  password VARCHAR(60) NOT NULL ,
  PRIMARY KEY (username));

CREATE TABLE user_roles (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  user_role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_username_role (role,username),
  KEY fk_username_idx (username),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username));

INSERT INTO users(username,password)
VALUES ('admin','$2a$10$eSIP.M78y8WuU0eX8UPVWO9fdFok9RsDp1HwEZsUyVtNMIchGLy4O'); --The password is also 'admin'

INSERT INTO user_roles (username, user_role)
VALUES ('admin', 'ROLE_ADMIN');